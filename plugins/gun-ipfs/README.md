# Plugin: `gun-ipfs`

- **status**: proof-of-concept, broken
- **type**: [transport plugin](../../docs/ARCHITECTURE.md#transport-plugins)

This plugin uses [GunDB](https://gun.eco/) as a source of information on current [IPFS CIDs](https://docs.ipfs.io/concepts/content-addressing/) of content, and retrieves that content using [JS IPFS](https://github.com/ipfs/js-ipfs).

## Configuration

*TBD*

## Operation

*TBD*
