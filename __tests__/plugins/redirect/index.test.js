const makeServiceWorkerEnv = require('service-worker-mock');

global.fetch = require('node-fetch');
jest.mock('node-fetch')

describe("plugin: redirect", () => {
  
  beforeEach(() => {
    Object.assign(global, makeServiceWorkerEnv());
    jest.resetModules();
    global.LibResilientPluginConstructors = new Map()
    init = {
        name: 'redirect',
        redirectStatus: 302,
        redirectStatusText: "Found",
        redirectTo: "https://redirected.example.org/subdir/"
    }
    LR = {
        log: (component, ...items)=>{
            console.debug(component + ' :: ', ...items)
        }
    }
  })
  
  test("it should register in LibResilientPluginConstructors", () => {
    init = {
        name: 'redirect',
        redirectTo: 'https://example.org/'
    }
    require("../../../plugins/redirect/index.js");
    expect(LibResilientPluginConstructors.get('redirect')(LR, init).name).toEqual('redirect');
  });
  
  test("it should fail with incorrect redirectTo config value", () => {
    init = {
        name: 'redirect',
        redirectTo: false
    }
    require("../../../plugins/redirect/index.js")
    expect.assertions(1)
    expect(()=>{
        LibResilientPluginConstructors.get('redirect')(LR, init)
    }).toThrow(Error);
  });
  
  test("it should fail with incorrect redirectStatus config value", () => {
    init = {
        name: 'redirect',
        redirectTo: 'https://example.org/',
        redirectStatus: 'incorrect'
    }
    require("../../../plugins/redirect/index.js")
    expect.assertions(1)
    expect(()=>{
        LibResilientPluginConstructors.get('redirect')(LR, init)
    }).toThrow(Error);
  });
  
  test("it should fail with incorrect redirectStatusText config value", () => {
    init = {
        name: 'redirect',
        redirectTo: 'https://example.org/',
        redirectStatusText: false
    }
    require("../../../plugins/redirect/index.js")
    expect.assertions(1)
    expect(()=>{
        LibResilientPluginConstructors.get('redirect')(LR, init)
    }).toThrow(Error);
  });
  
  test("it should register in LibResilientPluginConstructors without error even if all config data is incorrect, as long as enabled is false", () => {
    init = {
        name: 'redirect',
        redirectTo: false,
        redirectStatus: "incorrect",
        redirectStatusText: false,
        enabled: false
    }
    require("../../../plugins/redirect/index.js");
    expect(LibResilientPluginConstructors.get('redirect')(LR, init).name).toEqual('redirect');
  });
  
  test("it should return a 302 Found redirect for any request", async () => {
    init = {
        name: 'redirect',
        redirectTo: "https://redirected.example.org/subdirectory/"
    }
    
    require("../../../plugins/redirect/index.js");
        
    const response = await LibResilientPluginConstructors.get('redirect')(LR, init).fetch('https://resilient.is/test.json');
    
    
    //expect().toEqual()
    expect(response.url).toEqual('https://resilient.is/test.json')
    expect(response.status).toEqual(302)
    expect(response.statusText).toEqual('Found')
    expect(response.headers.get('location')).toEqual('https://redirected.example.org/subdirectory/test.json')
  })

});
